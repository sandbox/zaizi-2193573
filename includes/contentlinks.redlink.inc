<?php

/**
 * Helper method for enhancing filed values in RedLink.
 *
 * @param string $field_value
 *   text value of the content
 *
 * @return null|stdClass
 *   Response object if succesful,
 *   NULL otherwise.
 */
function contentlinks_redlink_field_value_enhance($field_value)
{
    $redlink_service = contentlinks_create_redlink_service();
    $content_analysis_api_wrapper = contentlinks_create_redlink_analysis_api_wrapper($redlink_service);

    try {
        return $content_analysis_api_wrapper->enhance($field_value, $redlink_service->getAnalysisChain());
    } catch (RedLinkServiceException $e) {
        return NULL;
    }
}

/**
 * Helper to call in hook_nodeapi implementation.
 *
 *
 * @param int $nid Node id.
 * @param string $type The type of content
 * @param string $field_machine_name The machine name of the field
 * @param stdClass $field The Standard field object
 *
 */
function contentlinks_nodeapi_field_enhance($nid, $type, $field_machine_name, $field, $update = FALSE)
{
    $response = contentlinks_redlink_field_value_enhance($field['und'][0]['value']);

    $enhanced_content = $response;
    //contentlinks_redlink_insert_field_mapping_record($nid, $type, $field_machine_name, $enhanced_content);
    return $enhanced_content;
    //contentlinks_redlink_insert_field_mapping_record($nid, $type, $field_machine_name, $enhanced_content);
}

/**
 * Creates a new RedLinkContentAPIWrapper instance, or fetches the cached one.
 *
 * @return RedLinkContentAPIWrapper
 *   RedLinkContentAPIWrapper instance with $redlink_service set as the
 *   service to use for requests.
 */
function contentlinks_create_redlink_analysis_api_wrapper($redlink_service)
{
    static $content_analysis_api_wrapper;

    // If it's never been set before, create new RedLinkContentAPIWrapper.
    if (!isset($content_analysis_api_wrapper)) {
        //Preparing the custom credentials.
        $credentials = new \RedLink\Credential\SecureCredentials($redlink_service->getApiTocken());
        $credentials->setSSLCertificates(REDLINK_ROOT_PATH . DIRECTORY_SEPARATOR . 'redlink-CA.pem');
        $content_analysis_api_wrapper = new \RedLink\Enhancer\RedLinkEnhanceImpl($credentials);

    }

    // Return either the "remembered" value or the one we just created.
    return $content_analysis_api_wrapper;
}

/**
 * Creates a new RedLinkService instance, or fetches the cached one.
 *
 * @return RedLinkService
 *   RedLinkService instance configured with settings set in module admin settings.
 */
function contentlinks_create_redlink_service()
{
    // "Remember" the value of the variable from last time we set it.
    $redlink_service = & drupal_static(__FUNCTION__);

    // If it's never been set before, create new AlfrescoService.
    if (empty($redlink_service)) {
        $scheme = variable_get('contentlinks_redlink_server_scheme', 'http');
        $host = variable_get('contentlinks_redlink_server_name', 'localhost');
        $port = variable_get('contentlinks_redlink_server_port', '80');
        $api_endpoint = variable_get('contentlinks_redlink_server_endpoint', 'api');
        $analysis_chain = variable_get('contentlinks_redlink_server_analysis_chain', 'anlysis_chain_name');
        $api_token = variable_get('contentlinks_redlink_server_api_token', 'xxxx');
        $redlink_service = new RedLinkService($scheme, $host, $port, $api_endpoint, $analysis_chain, $api_token);
    }

    // Return either the "remembered" value or the one we just created.
    return $redlink_service;
}

/**
 * Description: A function to insert a mapping record of an enhanced field
 * @param $nid
 * @param $type
 * @param $token
 * @param $field_machine_name
 * @param $field_index
 * @param $field_property
 * @param null $entitiy_json
 * @param null $micro_data_json
 */
function contentlinks_redlink_insert_field_mapping_record($nid, $type, $token, $field_machine_name, $field_index, $field_property, $entitiy_json = NULL, $micro_data_json = NULL)
{
    $record = (object)array(
        'nid' => $nid,
        'token' => $token,
        'type' => $type,
        'field_machine_name' => $field_machine_name,
        'field_index' => $field_index,
        'field_property' => $field_property,
        'enriched_data_entities' => $entitiy_json,
        'enriched_microdata' => $micro_data_json
    );

    try {
        if (drupal_write_record('contentlinks_field_enrichment_mapping_table', $record)) {
            error_log('inserted a mapping record');
        } else {
            error_log('error in inserting mapping record');
        }
    } catch (Exception $e) {
        error_log($e->getMessage());
    }
}

function contentlinks_redlink_update_field_mapping_record($nid, $type, $token, $microdata = array())
{
    db_update('contentlinks_field_enrichment_mapping_table')
        ->condition('token', $token, '=')
        ->fields(array('nid' => $nid, 'type' => $type, 'token' => NULL, 'enriched_microdata' => json_encode($microdata)))
        ->execute();
}

function contentlinks_get_mapping_records_for_nid($nid)
{
    $result = db_select('contentlinks_field_enrichment_mapping_table', 's')
        ->fields('s')
        ->condition('nid', $nid, '=')
        ->execute()
        ->fetchAll();
    return $result;
}

function contentlinks_is_there_mapping_records_for_nid($nid)
{
    if (count(contentlinks_get_mapping_records_for_nid($nid))) {
        return true;
    } else {
        return false;
    }
}

function contentlinks_delete_mapping_records_for_nid($nid)
{
    db_delete('contentlinks_field_enrichment_mapping_table')
        ->condition('nid', $nid)
        ->execute();
}

function contentlinks_get_entity_json_for_node_by_nid($nid)
{
    $result = contentlinks_get_mapping_records_for_nid($nid);
    $entities = array();
    foreach ($result as $row) {
        $entities[$row->field_machine_name][$row->field_index][$row->field_property]['entities'] = json_decode($row->enriched_data_entities, true);
    }
    return $entities;

}

function contentlinks_get_mapping_records_by_token($token)
{
    $result = db_select('contentlinks_field_enrichment_mapping_table', 's')
        ->fields('s')
        ->condition('token', $token, '=')
        ->execute()
        ->fetchAll();
    return $result;
}

function contentlinks_is_there_mapping_records_for_token($token)
{
    if (count(contentlinks_get_mapping_records_by_token($token))) {
        return true;
    } else {
        return false;
    }
}

function contentlinks_delete_mapping_records_by_field_properties_and_nid($nid, $field_machine_names, $field_indexes, $field_properties)
{
    db_delete('contentlinks_field_enrichment_mapping_table')
        ->condition('nid', $nid)
        ->condition('field_machine_name', $field_machine_names, 'IN')
        ->condition('field_index', $field_indexes, 'IN')
        ->condition('field_property', $field_properties, 'IN')
        ->execute();
}
