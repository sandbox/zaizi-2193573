<?php

/**
 * Description: A function to get all fields that are qualified
 * to be sent to RedLink to be enhanced
 * @param string $type the content type
 * @return mixed
 */
function get_contentlinks_qualified_fields_by_type($type) {
  $fields_array = get_content_types_text_field_array_for_type($type);

  $qualified_fields = array();
  foreach ($fields_array['fields'] as $field) {
    $field_key = CONTENTLINKS_FIELD_PREFIX . $type . "_" . $field['machine_name'];
    if (variable_get($field_key) == 1) {
      $qualified_fields[] = $field['machine_name'];
    }
  }
  return $qualified_fields;
}

/**
 * Description: A module to get content types that have qualified fields
 * to be enriched
 * @return mixed
 *
 *
 */
function get_contentlinks_qualified_types() {

  $content_fields = get_content_types_text_field_array();

  $qualified_types = array();
  foreach ($content_fields as $type) {
    $type_key = CONTENTLINKS_FIELD_PREFIX . $type['machine_name'];
    if (variable_get($type_key) == 1) {
      $qualified_types[] = $type['machine_name'];
    }
  }
  return $qualified_types;
}

/**
 * Desciption: A module that will get you a array of text fields for particular content type
 *
 * @param type $type  the machine_name of the content type
 * @return type
 */
function get_content_types_text_field_array_for_type($type) {
  $passable_field_modules = explode(CONTENTLINKS_ARRAY_CONSTANT_DELIMETER, CONTENTLINKS_PASSABLE_FIELD_MODULES);
  $passable_field_types = explode(CONTENTLINKS_ARRAY_CONSTANT_DELIMETER, CONTENTLINKS_PASSABLE_FIELD_TYPES);
  $fileds_array = array();
  $field_list = field_info_instances('node', $type); //get field list for content type
  $file_field_count = 0;
  foreach ($field_list as $field_key => $field_value) {
    if (in_array($field_value['widget']['module'], $passable_field_modules) && in_array($field_value['widget']['type'], $passable_field_types)) {

      $fileds_array ['fields'][$file_field_count]['label'] = $field_value['label'];
      $fileds_array ['fields'][$file_field_count]['machine_name'] = $field_key;
      $file_field_count++;
    }
  }

  return $fileds_array;
}

/**
 * @param $entity_annotations
 * @return array
 */
function restructure_entity_annotations($entity_annotations) {
  $search = array();
  $replace = array();
  $starts = array();
  $ends = array();
  $valid_types = array();
  $entities = array();
  $entity_count = array();
  $uris = array();

  foreach ($entity_annotations as $entity_annotation) {
    $entity_reference = $entity_annotation->getEntityReference();
    $text_annotations = $entity_annotation->getRelations();
    $resource_url = $entity_reference->getUri();


    foreach ($text_annotations as $text_annotation) {
      $keyword = $text_annotation->getSelectedText();
      $current_start = $text_annotation->getStarts();
      $type_array = get_real_and_valid_type_for_entity($entity_annotation, $entity_reference);
      if ($type_array['valid_type'] != CONTENTLINKS_ENTITY_TYPE_EMPTY) { //if the type is not empty
        if (!in_array($current_start, $starts)) {
          $starts[] = $current_start;
          $valid_types[$current_start] = $type_array['valid_type'];
          $search[$current_start] = $keyword;
          $uris[$current_start][] = $resource_url;
          $replace[$current_start] = '&nbsp;<div class="contentlinks-wrapper text-' . $current_start . ' contentlinks-entity-initial" >' . $keyword . '</div>';
          $entity_count[$current_start] = 1;
          contentlinks_set_entity_for_entity_annotation($entities, $entity_annotation, $entity_reference, $type_array, $resource_url, $current_start, $entity_count);
          $ends[$current_start] = $text_annotation->getEnds();
        }
        else {
          $current_valid_type = $valid_types[$current_start];
          $valid_types[$current_start] = $type_array['valid_type'];
          $type_updated = FALSE;
          if (is_text_annotation_type_updated($current_valid_type, $valid_types[$current_start])) {
            $replace[$current_start] = '&nbsp;<div class="contentlinks-wrapper text-' . $current_start . ' contentlinks-entity-initial" >' . $keyword . '</div>';
            $entity_count[$current_start] = 1;
            contentlinks_set_entity_for_entity_annotation($entities, $entity_annotation, $entity_reference, $type_array, $resource_url, $current_start, $entity_count);
            $type_updated = TRUE;
          }
          if (is_new_entity($resource_url, $entity_reference, $uris[$current_start]) && !$type_updated) {
            $entity_count[$current_start]++;
            contentlinks_set_entity_for_entity_annotation($entities, $entity_annotation, $entity_reference, $type_array, $resource_url, $current_start, $entity_count);
          }
          $uris[$current_start][] = $resource_url;
        }
      }

      break; // go through only one text annotation

    }

  }
  die;
  return array(
    'search' => $search,
    'replace' => $replace,
    'entities' => $entities,
    'starts' => $starts,
    'ends' => $ends
  );
}

/**
 * A function to set the entity array
 * @param array $entities pass by reference array
 * @param stdClass $entity_annotation
 * @param stdClass $entity_reference
 * @param array $type_array
 * @param string $resource_url
 * @param int $current_start
 * @param array $entity_count
 */
function contentlinks_set_entity_for_entity_annotation(&$entities, $entity_annotation, $entity_reference, $type_array, $resource_url, $current_start, $entity_count) {
  $real_type = $type_array['real_type'];
  $db_type = $type_array['db_type'];
  $valid_type = $type_array['valid_type'];

  $entities[$current_start][$entity_count[$current_start]]['entity_uri'] = $entity_annotation->getUri();
  $entities[$current_start][$entity_count[$current_start]]['label'] = $entity_annotation->getEntityLabel();
  $entities[$current_start][$entity_count[$current_start]]['uri'] = $resource_url;
  $entities[$current_start][$entity_count[$current_start]]['type'] = $real_type;
  $entities[$current_start][$entity_count[$current_start]]['valid_type'] = $valid_type;
  $entities[$current_start][$entity_count[$current_start]]['db_type'] = contentlinks_get_schema_org_mapping($db_type);
  $entities[$current_start][$entity_count[$current_start]]['description'] = get_entity_property_value_by_key($entity_reference, 'comment', 'en');
  $entities[$current_start][$entity_count[$current_start]]['thumb'] = get_entity_property_value_by_key($entity_reference, 'depiction');
  $entities[$current_start][$entity_count[$current_start]]['properties'] = set_properties_for_entity_using_mappings($entity_reference);

}


function set_properties_for_entity_using_mappings($entity_ref) {
  $sr_property_mapping_array = json_decode(variable_get("contentlinks_sr_property_mappings"), TRUE);
  $property_array = array();
  $property_val = '';
  $multiple_lang_properties = array('matchLable', 'comment', 'label');
  foreach ($sr_property_mapping_array as $property) {
    if ($property['redlink'] == 'uri') {
      $property_val = $entity_ref->getUri();
    }
    elseif (in_array($property['redlink'], $multiple_lang_properties)) {
      $property_val = get_entity_property_value_by_key($entity_ref, $property['redlink'], 'en', FALSE);
    }
    else {
      $property_val = get_entity_property_value_by_key($entity_ref, $property['redlink'], NULL, TRUE);
    }
    if (!empty($property_val)) {
      $property_array[$property['schema_org']] = $property_val;
    }

  }
  return $property_array;
}

/**
 * A function to create the enhanced string with enhancement
 * @param $content
 * @param $restructured_annotation_array
 * @return string
 */
function construct_enhaced_string($content, $restructured_annotation_array) {
  $replace = $restructured_annotation_array['replace'];
  $starts = $restructured_annotation_array['starts'];
  $ends = $restructured_annotation_array['ends'];
  sort($starts);
  $count = 0;
  $content_length = strlen($content);
  $enhanced_str = '';
  $prev_val = 0;
  foreach ($starts as $val) {
    if ($count == 0) {
      $cut_length = $val - 0;
      $enhanced_str .= substr($content, 0, $cut_length);
      $enhanced_str .= $replace[$val];

    }
    else {
      if ($ends[$prev_val] < $ends[$val]) {
        $cut_length = $val - $ends[$prev_val];
        $enhanced_str .= substr($content, $ends[$prev_val], $cut_length);
        $enhanced_str .= $replace[$val];
      }
    }

    $prev_val = $val;
    $count++;
  }

  $last_str_length = $content_length - $ends[$prev_val];
  $enhanced_str .= substr($content, $ends[$prev_val], $last_str_length); // concatinate the end of the string

  return $enhanced_str;
}

/**
 * A function to enhance the content using a ajax call
 * The function is called through the plugin written for ckeditor
 * @param string $args
 */
function contentlinks_ajax_content_enhance($args = 'all') {
  module_load_include('inc', 'contentlinks', 'includes/contentlinks.redlink');
  if (function_exists('normalizer_normalize')) {
    $content = normalizer_normalize(strip_tags($_POST['content']));
  }
  else {
    $content = utf8_decode(strip_tags($_POST['content']));
  }

  $token = strip_tags($_POST['token']);
  $type = strip_tags($_POST['type']);
  $field_machine_name = strip_tags($_POST['field_machine_name']);
  $field_index = strip_tags($_POST['field_index']);
  $field_property = strip_tags($_POST['field_property']);
  $response = contentlinks_redlink_field_value_enhance($content);
  $entity_annotations = $response->getEntityAnnotations();
  $the_entities = array();
  foreach ($entity_annotations as $entity_annotation) {
    $entity = $entity_annotation->getEntityReference();
    $the_entities[$entity->getUri()] = $entity;
  }

  $result = restructure_entity_annotations($entity_annotations);
  $enhanced_str = construct_enhaced_string($content, $result);
  //nid is NULL microdata is also NULL
  contentlinks_redlink_insert_field_mapping_record(NULL, $type, $token, $field_machine_name, $field_index, $field_property, json_encode($result['entities']), json_encode($the_entities));


  echo json_encode(array(
    'enhanced_content' => $enhanced_str,
    'entities' => $result['entities'],
    'color_css' => contentlinks_get_color_coded_css()
  ));
  exit();
}

/**
 * A function to remove enhanced content when the browser is being closed or the location is updated
 * The function is called when the browser is closed, or the location is updated
 * @param string $args
 */
function contentlinks_ajax_remove_content_enhance($args = 'all') {
  // Contents that were enhanced, but which the user wishes not to save should be deleted
  if (isset($_POST['contentlinks_token'])) {
    $contentlinks_token = $_POST['contentlinks_token'];

    // Records with the contentlinks_token will be removed
    db_delete('contentlinks_field_enrichment_mapping_table')
      ->condition('token', $contentlinks_token)
      ->execute();
  }
}

/**
 * A function to get the last path argument from a URL
 * @param $url
 * @return mixed|string
 *
 */
function get_the_last_path_argument_from_url($url) {
  $url_path_array = explode('/', $url);

  $last_path_argument = array_pop($url_path_array);
  if (!is_null($last_path_argument)) {
    return preg_replace('/[0-9]*/', '', $last_path_argument);
  }
  else {
    return '';
  }

}

/**
 * A function to get the real type from an array of entityTypes
 * @param $entity_annotation
 * @param mixed $entity_reference
 * @return array
 */
function get_entity_type_array_from_entity_type_urls($entity_annotation, $entity_reference) {
  $entity_types = $entity_annotation->getEntityTypes();
  $entity_types = array_unique(array_merge($entity_types, get_entity_property_value_by_key($entity_reference,'type', NULL, false)));

//dpr($entity_types);
  $real_types = array('url' => array(), 'type' => array());

  if (is_array($entity_types)) {
    foreach ($entity_types as $entity_type) {
      $real_types['url'][] = $entity_type;
      $real_types['type'][] = get_the_last_path_argument_from_url($entity_type);

    }
  }
  return $real_types;
}

/**
 * A function to get the real entity type (out of valid types in constant CONTENTLINKS_VALID_ENTITY_TYPES)
 * @param mixed $entity_annotation
 * @param mixed $entity_reference
 * @return string
 */
function get_real_and_valid_type_for_entity($entity_annotation, $entity_reference) {
  $entity_types = $entity_annotation->getEntityTypes();
  $real_type = CONTENTLINKS_ENTITY_TYPE_EMPTY;
  $valid_type = CONTENTLINKS_ENTITY_TYPE_EMPTY;
  $db_type = '';
  // if the $type is still empty go through the entityTypes as well

  if (is_array($entity_types) && count($entity_types) > 0 ) {
    $types_from_urls = get_entity_type_array_from_entity_type_urls($entity_annotation, $entity_reference);

    if (is_array($types_from_urls)) {
      $real_type = $types_from_urls['type']; // the types from the entity
      $db_type = $types_from_urls['url'];

      $selected_type = contentlinks_select_candidate_type_from_array($types_from_urls);
      $valid_type = label_real_type_by_valid_type($selected_type);
    }
  }
  if (is_null($real_type)) {
    $real_type = CONTENTLINKS_ENTITY_TYPE_EMPTY;
  }

  $return = array(
    'db_type' => $db_type,
    'real_type' => $real_type,
    'valid_type' => $valid_type
  );

  return $return;
}

/**
 * A function to assign a valid type according to the real type passed in
 * @param $real_type string The real
 * @return string
 */
function label_real_type_by_valid_type($real_type) {
  if (!empty($real_type)) {
    switch ($real_type) {
      case CONTENTLINKS_ENTITY_TYPE_PLACE:
      case CONTENTLINKS_ENTITY_TYPE_PERSON:
      case CONTENTLINKS_ENTITY_TYPE_ORGANISATION:
      case CONTENTLINKS_ENTITY_TYPE_ORGANIZATION:
        $valid_type = $real_type;
        break;
      default:
        $valid_type = CONTENTLINKS_ENTITY_TYPE_OTHER;
    }
  }
  else {
    $valid_type = CONTENTLINKS_ENTITY_TYPE_EMPTY;
  }
  return $valid_type;
}

/**
 * A function to get the style color according to the entity type
 * @param $type
 * @return string
 */
function get_style_for_entity_type($type) {
  $contentlinks_entity_color_codes = variable_get("contentlinks_entity_color_codes");

  $style = '';
  switch ($type) {
    case CONTENTLINKS_ENTITY_TYPE_PLACE:
      $style = isset($contentlinks_entity_color_codes[CONTENTLINKS_ENTITY_TYPE_PLACE]) ? $contentlinks_entity_color_codes[CONTENTLINKS_ENTITY_TYPE_PLACE] : '#05B773';
      break;
    case CONTENTLINKS_ENTITY_TYPE_PERSON:
      $style = isset($contentlinks_entity_color_codes[CONTENTLINKS_ENTITY_TYPE_PERSON]) ? $contentlinks_entity_color_codes[CONTENTLINKS_ENTITY_TYPE_PERSON] : '#7BC7F7';
      break;
    case CONTENTLINKS_ENTITY_TYPE_ORGANISATION:
    case CONTENTLINKS_ENTITY_TYPE_ORGANIZATION:
      $style = isset($contentlinks_entity_color_codes[CONTENTLINKS_ENTITY_TYPE_ORGANISATION]) ? $contentlinks_entity_color_codes[CONTENTLINKS_ENTITY_TYPE_ORGANISATION] : '#FFA642';
      break;
    case CONTENTLINKS_ENTITY_TYPE_OTHER:
      $style = isset($contentlinks_entity_color_codes[CONTENTLINKS_ENTITY_TYPE_OTHER]) ? $contentlinks_entity_color_codes[CONTENTLINKS_ENTITY_TYPE_OTHER] : '#FFEFBF';
      break;
    default: //Empty
      $style = isset($contentlinks_entity_color_codes[CONTENTLINKS_ENTITY_TYPE_EMPTY]) ? $contentlinks_entity_color_codes[CONTENTLINKS_ENTITY_TYPE_EMPTY] : '#E6FC25';
  }

  return $style;
}

/**
 * Check if the text annotation type is updated
 * @param $current_type
 * @param $new_type
 * @return bool
 */
function is_text_annotation_type_updated($current_type, $new_type) {
  if ($current_type == $new_type || is_text_annotation_type_valid($current_type)) {
    return FALSE;
  }
  else {
    if ($new_type == CONTENTLINKS_ENTITY_TYPE_EMPTY) {
      return FALSE;
    }
    else {
      return TRUE;
    }
  }
}

/**
 * check if the type assigned is a valid type. Other and Empty are not valid types
 * @param $type
 * @return bool
 */
function is_text_annotation_type_valid($type) {
  if ($type != CONTENTLINKS_ENTITY_TYPE_EMPTY && $type != CONTENTLINKS_ENTITY_TYPE_OTHER) {
    return TRUE;
  }
  else {
    return FALSE;
  }
}

/**
 * Get #samaAs and #seeAlso property values for an entity
 * @param $entity_ref
 * @return array
 */
function get_entity_sameas_and_seealso_property_values($entity_ref) {
  $property_keys = $entity_ref->getProperties();
  $property_val_array = array();

  foreach ($property_keys as $key) {
    if (substr_count($key, CONTENTLINKS_SAME_AS_PROPERTY_KEY) > 0 || substr_count($key, CONTENTLINKS_SEE_ALSO_PROPERTY_KEY) > 0) {
      $property_val_array = array_unique(array_merge($entity_ref->getValues($key), $property_val_array));
    }
  }

  return $property_val_array;
}


/**
 * A function to get the property values of a entity reference
 * @param mixed $entity_ref
 * @param string $property_key
 * @param bool $get_first_val_only
 * @return array
 */
function get_entity_property_value_by_key($entity_ref, $property_key, $lang = NULL, $get_first_val_only = TRUE) {
  $property_keys = $entity_ref->getProperties();
  $property_val_array = array();

  foreach ($property_keys as $key) {
    if (substr_count($key, $property_key) > 0) {
      if (is_null($lang)) {
        if ($get_first_val_only) {
          $property_val_array = $entity_ref->getFirstPropertyValue($key);
        }
        else {
          $property_val_array = $entity_ref->getValues($key);
        }
      }
      else {
        $property_val_array = $entity_ref->getValue($key, $lang);
      }
    }
  }
  return $property_val_array;
}

/**
 * Check if the current entity is a duplicate entity using the resource URI and #samaAs and #seeAlso property arrays
 * @param $resource_url
 * @param $entity_ref
 * @param $uri_array
 * @return bool
 */
function is_new_entity($resource_url, $entity_ref, $uri_array) {
  $property_value_array = get_entity_sameas_and_seealso_property_values($entity_ref);
  $property_value_array[] = $resource_url;
  $intersection = array_intersect($uri_array, $property_value_array);

  if (count($intersection) > 0) {
    return FALSE;
  }
  else {
    return TRUE;
  }
}

/**
 * A function to remove old mapping if any and update current entity mappings
 * @param mixed $node
 */
function contentlinks_update_mapping_records($node) {
  module_load_include('inc', 'contentlinks', 'includes/contentlinks.redlink');
//    module_load_include('php', 'contentlinks', 'RedLinkPHPSDK/src/bootstrap');
  $qualified_types = get_contentlinks_qualified_types();
  if (in_array($node->type, $qualified_types)) {

    if (isset($node->contentlinks_token)) {
      $token = $node->contentlinks_token;
      $enriched_microdata = '';
      if (isset($node->nid)) {
        $records_for_token = contentlinks_get_mapping_records_by_token($token); //get mappings for the current token
        if (count($records_for_token)) {
          if (contentlinks_is_there_mapping_records_for_nid($node->nid)) {
            $field_machine_names = array();
            $field_indexes = array();
            $field_properties = array();
            foreach ($records_for_token as $row) {
              $field_machine_names[] = $row->field_machine_name;
              $field_indexes[] = $row->field_index;
              $field_properties[] = $row->field_property;
            }
            //delete old mapping records for the same node id
            contentlinks_delete_mapping_records_by_field_properties_and_nid($node->nid, $field_machine_names, $field_indexes, $field_properties);
          }

          reset($records_for_token);
          $record = end($records_for_token);
          $enriched_microdata = $record->enriched_microdata;
        }
      }


      $enriched_microdata = json_decode($enriched_microdata, TRUE);

      $selected_entities = array();
      $newEntities = array();
      if (isset($node->selected_entities) && !empty($node->selected_entities)) {
        $selected_entities = explode(" ", $node->selected_entities);
      }

      foreach ($selected_entities as $uri) {
        $newEntities[$uri] = $enriched_microdata[$uri];
      }

      contentlinks_redlink_update_field_mapping_record($node->nid, $node->type, $token, $newEntities);
    }

  }
}

/**
 * A function to replace dbpedia or freebase types with schema.org types
 * according ot the mapping given
 * @param string $dbType
 * @return bool|int|string
 */
function contentlinks_get_schema_org_mapping($dbType) {

  if (substr_count($dbType, CONTENTLINKS_SCHEMA_ORG_URL) > 0) {
    return $dbType;
  }
  $schema_org_mapped_entity_list = variable_get("contentlinks_schema_org_mapped_entity_list");

  foreach ($schema_org_mapped_entity_list as $mapped_entity) {
    foreach ($mapped_entity as $schema_org_entity => $entity) {
      if ($entity == $dbType) {
        return $schema_org_entity;
      }
    }
  }
  return FALSE;
}

/**
 * A function to get all schema.org types (If the service is not available there will be a warning message)
 * @param bool $is_mapping_form
 * @return array|bool|float|int|string
 */
function get_schema_org_properties($is_mapping_form = FALSE) {
  $properties =  & drupal_static(__FUNCTION__);
  if (!isset($properties)) {
//        module_load_include('php', 'contentlinks', 'RedLinkPHPSDK/src/bootstrap');

    try {
      $client = new Guzzle\Http\Client('http://schema.rdfs.org', array(
        'request.options' => array(
          'timeout' => 20,
          'connect_timeout' => 20
        )
      ));
      $response = $client->get('/all.json')->send();

      if ($response->getStatusCode() != 200) {
        throw new \RuntimeException("Enhancement failed: HTTP error code " . $response->getStatusCode());
      }
      else {
        $properties = $response->json();
      }

      return $properties;

    }
    catch (Exception $e) {
      if ($is_mapping_form) {
        drupal_set_message('http://schema.rdfs.org service not available at the moment, Due to this schema.org mapping is not possible. However you can manually map the entities', 'warning');
      }
      else {
        drupal_set_message('http://schema.rdfs.org service not available at the moment, Due to this further enhancing will be disabled but it will not disable RedLink enhancements.', 'warning');
      }


    }
  }
}

function contentlinks_get_contentlinks_tags_value_for_node($term_id_array) {
  $contentlinks_tag_str = '';
  $terms = array();
  if (!empty($term_id_array) && !is_null($term_id_array)) {
    foreach ($term_id_array as $term) {
      $terms[] = $term['tid'];
    }
    $term_array = taxonomy_term_load_multiple($terms);
    $count = 0;
    foreach ($term_id_array as $term) {
      $parent = taxonomy_get_parents($term['tid']);
      if (!empty($parent)) {
        $key_array = array_keys($parent);
        $key = array_pop($key_array);
        $parent = $parent[$key];

        if ($count == 0) {
          $contentlinks_tag_str .= $parent->name . '->' . $term_array[$term['tid']]->name;
        }
        else {
          $contentlinks_tag_str .= ',' . $parent->name . '->' . $term_array[$term['tid']]->name;
        }

        $count++;
      }

    }
  }

  return $contentlinks_tag_str;
}

/**
 * Callback function
 * @param string $args
 */
function contentlinks_ajax_remove_schema_org_mapping($args = 'all') {
  if (isset($_POST['index'])) {
    $schema_org_mapped_entity_list = variable_get("contentlinks_schema_org_mapped_entity_list");

    if (!isset($schema_org_mapped_entity_list[$_POST['index']])) {
      $result = array('error_code' => 1);
      echo json_encode($result);
      exit;
    }
    else {
      unset($schema_org_mapped_entity_list[$_POST['index']]);
      variable_set("contentlinks_schema_org_mapped_entity_list", $schema_org_mapped_entity_list);
      $result = array('error_code' => 0);
      echo json_encode($result);
      exit;
    }
  }
  else {
    $result = array('error_code' => 1);
    echo json_encode($result);
    exit;
  }
}

/**
 * Callback Function
 * @param $args
 */
function contentlinks_ajax_remove_sr_property_mapping($args = 'all') {
  $sr_property_mapping_array = json_decode(variable_get("contentlinks_sr_property_mappings"), TRUE);
  if (isset($_POST['prop_index']) && isset($sr_property_mapping_array[$_POST['prop_index']])) {
    unset($sr_property_mapping_array[$_POST['prop_index']]);
    variable_set("contentlinks_sr_property_mappings", json_encode($sr_property_mapping_array));
    $result = array('error_code' => 0);
    echo json_encode($result);
    exit;
  }
  else {
    $result = array('error_code' => 1);
    echo json_encode($result);
    exit;
  }
}

/**
 * Function to insert/update selected node entities
 * @param $node
 */
function contentlinks_update_field_selected_entities($node) {

  if (isset($node->contentlinks_selected_entities) && !empty($node->contentlinks_selected_entities)) {

    $entities = explode(",", $node->contentlinks_selected_entities);
    $selected_entities = array();

    foreach ($entities as $entity) {
      $entity = explode("->", $entity);

      if (!isset($selected_entities[$entity[0]])) {
        $selected_entities[$entity[0]] = array();
      }

      if (!in_array($entity[1], $selected_entities[$entity[0]])) {
        array_push($selected_entities[$entity[0]], $entity[1]);
      }
    }

    foreach ($selected_entities as $key => $entities) {
      $field_info = explode("-", $key);

      db_delete(CONTENTLINKS_NODE_ENTITIES_TABLE)
        ->condition('nid', $node->nid)
        ->condition('field_machine_name', $field_info[0])
        ->condition('field_index', $field_info[1])
        ->condition('field_property', $field_info[2])
        ->execute();

      foreach ($entities as $entity) {

        $variables[] = $node->nid;
        $variables[] = $field_info[0];
        $variables[] = $field_info[1];
        $variables[] = $field_info[2];
        $variables[] = $entity;

        db_insert(CONTENTLINKS_NODE_ENTITIES_TABLE)
          ->fields(array(
            'nid' => $node->nid,
            'field_machine_name' => $field_info[0],
            'field_index' => $field_info[1],
            'field_property' => $field_info[2],
            'entity_uri' => $entity
          ))
          ->execute();
      }
    }
  }
}

/**
 * Function to remove selected node entities
 * @param $node
 */
function contentlinks_remove_field_selected_entities($node) {

  db_delete(CONTENTLINKS_NODE_ENTITIES_TABLE)
    ->condition('nid', $node)
    ->execute();
}

/**
 * Function to get all selected entities for a node
 * @param $node
 */
function contentlinks_get_selected_entity_value_for_node($form) {

  $selected_entity_value = "";

  if (isset($form['#node']->nid)) {
    $node = $form['#node']->nid;
    $selected_entities = db_select(CONTENTLINKS_NODE_ENTITIES_TABLE, 'c')
      ->fields('c')
      ->condition('nid', $node, '=')
      ->execute()
      ->fetchAll();

    foreach ($selected_entities as $entity) {
      if ($selected_entity_value != "") {
        $selected_entity_value .= ",";
      }
      $selected_entity_value .= $entity->field_machine_name . "-" . $entity->field_index . "-" . $entity->field_property . "->" . $entity->entity_uri;
    }
  }

  return $selected_entity_value;
}

function contentlinks_get_more_nodes_like_this($node) {
  static $related_node_links;
  if (!isset($related_node_links)) {
    $related_node_links[$node] = & drupal_static(__FUNCTION__);
  }
  if (!isset($related_node_links[$node])) {
    $related_node_links[$node] = array();
    $node_entity_uri = db_select(CONTENTLINKS_NODE_ENTITIES_TABLE, 'c')
      ->fields('c')
      ->condition('nid', $node, '=')
      ->execute()
      ->fetchAll();

    $applicable_entities = array();


    foreach ($node_entity_uri as $uri) {
      array_push($applicable_entities, $uri->entity_uri);
    }

    if (count($applicable_entities) > 0) {

      $related_nodes = db_query(' select * from
                                    (select distinct nid, count(entity_uri) as matches from {contentlinks_node_entities_table}
                                    where entity_uri in (
                                    select entity_uri from {contentlinks_node_entities_table}
                                    where nid = :nid)
                                    and nid != :nid
                                    group by nid
                                    order by matches desc) matching_entities
                                    where matching_entities.matches >= :matches'
        , array(
          ':nid' => $node,
          ':matches' => CONTENTLINKS_MORE_LIKE_THIS_THRESHOLD
        ));

      $related_node_links[$node] = array();

      foreach ($related_nodes as $relative_node) {
        $db_node = node_load($relative_node->nid);
        array_push($related_node_links[$node], l($db_node->title . " (" . $relative_node->matches . ")", 'node/' . $db_node->nid));
      }

      if (count($related_node_links[$node]) > 0) {
        $related_node_links[$node] = theme('item_list', array('items' => $related_node_links[$node]));
      }
      else {
        $related_node_links[$node] = "";
      }
    }
  }
  return $related_node_links;
}

/**
 * A function to select the candidate type out of the type array
 * @param $types_from_urls
 * @return array
 */
function contentlinks_select_candidate_type_from_array($types_from_urls) {
  //filter the array by removing schema.org and w3.org urls
  $filtered_array_urls = array_filter($types_from_urls['url'], 'contentlinks_is_generic_type');
  //if the array still has elements
    if(count($filtered_array_urls) > 0){
      //get the corresponding types using the filtered urls array
      $filtered_array_types = array_intersect_key($types_from_urls['type'], $filtered_array_urls);
      return contentlinks_get_highest_occuring_url_and_type($filtered_array_types, $filtered_array_urls);
    } else {
      return contentlinks_get_highest_occuring_url_and_type($types_from_urls['type'], $types_from_urls['url']);
    }

}

/**
 * Callback function for the array_filter to remove schema.org or w3.org urls
 * @param string $element
 * @return bool if the element should stay or be filtered out
 */
function contentlinks_is_generic_type($element) {
  //check if the current element has schema.org or w3.org url
  if(substr_count($element , CONTENTLINKS_SCHEMA_ORG_URL) > 0 || substr_count($element , CONTENTLINKS_W3_ORG_URL) > 0) {
    return false;
  } else {
    return true;
  }
}

  /**
   * A function to find the highest occuring url element by type and and the
   * corresponding url
   * @param array $array_types
   * @param array $array_urls
   * @return array
   */
function contentlinks_get_highest_occuring_url_and_type($array_types, $array_urls) {
  // get the occurance number of each element
  $occurance_count = array_count_values($array_types);
  //sort the array in decending order so that the highest occurance is at the top
  arsort($occurance_count);
  $highest_occuring_element = array_shift($occurance_count);
  //get the key of the highest occuring element
  $highest_occuring_element_key = array_search($highest_occuring_element, $array_types);
  //get the corresponding URL from the URL array using the key
  $highest_occuring_element_url = $array_urls[$highest_occuring_element_key];
  return array('url' => $highest_occuring_element_url, 'type' => $highest_occuring_element);
}

