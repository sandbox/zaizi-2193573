User Manual for ContentLinks module

Installation

Dependencies
There are 3 contributed module dependencies for this module
Ckeditor: https://drupal.org/project/ckeditor
Libraries: https://drupal.org/project/libraries
Jquery Update: https://drupal.org/project/jquery_update

Also there is a 3rd party dependency for the module, which is the RedLink PHP SDK
You can use GIT to get the module Instructions are given here
http://dev.redlink.io/sdk/#php-installation

Add the SDK to the sites/all/libraries folder

If you have successfully managed to install and configure all the dependencies properly, please carefully  follow the next steps in order to install and use the ContentLinks module
STEP1:
Download the module from here: https://drupal.org/project/contentlinks
STEP2:
Add the module to the sites/all/modules/ folder
STEP3:
Add the RedLink PHP SDK to the libraries folder. The library folder name should be RedLinkPHPSDK
STEP4:
Got to /admin/modules and enable the module

Configuration steps
STEP1:
ContentLinks Server integration
	This is where you configure the RedLink API with Drupal
STEP2:
	ContentLinks Content type selection: This is where the user  decides what fields in what content types are selected for enhancing
STEP3:
	ContentLinks Schema.org mapping: This interface will allow the users to map other knowledge base entities to schema.org entities.
Ex: http://freebase.org/person -> http://schema.org/Person
the reason for this mapping is to make more sense for search engines since schema.org is the most recognized by search engines when it comes to structured data within HTML markup. On-page markup enables search engines to understand the information on web pages and provide richer search results in order to make it easier for users to find relevant information on the web. Markup can also enable new tools and applications that make use of the structure.

STEP4:
	ContentLinks schema.org and RedLink property mapping: Map other knowledge base properties to schema.org properties
Ex: comment - > description
The above example will make sure any property value with the label comment will be mapped to a property called description. The result of mapping properties will result in a markup siimlar to the below

<div itemprop="video" itemscope itemtype="http://schema.org/VideoObject">
  <h2>Video: <span itemprop="name">Interview with the Foo Fighters</span></h2>
  <meta itemprop="duration" content="T1M33S" />
  <meta itemprop="thumbnail" content="foo-fighters-interview-thumb.jpg" />
  <object ...>
    <param ...>
    <embed type="application/x-shockwave-flash" ...>
  </object>
  <span itemprop="description">Catch this exclusive interview with
    Dave Grohl and the Food Fighters about their new album, Rope.</span>
</div>

STEP5:
	Entity Color code: In the current version the module only handles 3 specific entity types namely, Person , Place and Organization. All other types will be categorized into Other category. All entities that do not have a type will be labeled as Empty.

STEP6:
	Configure CKEditor:
--First enable the  CKEditor for plain text mode as well.
--In All CKEditor profiles got to advanced options and make sure you add the following code to 	Custom JavaScript configuration
	config.allowedContent=true;
--Enabling plugins- Make sure you enable the following plugins in al profiles
A ckeditor plugin to enhance your content using RedLink API
A ckeditor plugin to show entities of selected text annotation
-- Drag the enhancer button to the Used buttons in all profiles

STEP7:
	Alter the text formats in order to include structure data markup in HTML. This step is needed to make sure the structured data markup is valid whenever you markup your HTML. If this step is not configured properly. The markup will be removed on the view mode of the content.
Remove the following filters for all formats. Do it for all text formats you wish to enhance













