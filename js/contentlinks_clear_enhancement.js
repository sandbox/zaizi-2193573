/**
 * Created by dphilips on 12/6/13.
 */

jQuery(window).bind(
    "beforeunload",
    function() {
        if(Drupal.settings.contentlinks.enhanced != undefined && Drupal.settings.contentlinks.enhanced == true){
            return "Closing the page will result in enhanced content not being saved! do you wish to continue?";
        }
    }
);

jQuery(document).ready(function(){
    jQuery('form').submit(function() {
        jQuery(window).unbind("beforeunload");
    });
});


jQuery( window ).unload(function() {
    if(Drupal.settings.contentlinks.enhanced != undefined && Drupal.settings.contentlinks.enhanced == true){
        jQuery.ajax({
            type: 'POST',
            url: Drupal.settings.basePath + "contentlinks/remove_content_enhance",
            async: false,
            data: {contentlinks_token: Drupal.settings.contentlinks.contentlinks_token}
        });
    }
});