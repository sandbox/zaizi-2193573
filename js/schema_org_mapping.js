/* This module handles Ajax Calls for Schema.org mappings*/

jQuery(document).ready(function($){
    $('.remove_enetity_mapping').click(function(){
        var index = $(this).parent().parent().attr('id').replace('schema_org_mapping_row_','');
        console.log(index);
        jQuery.post("/contentlinks/remove_schema_org_mapping",{index:index}, function(data){
            data = eval("(" + data + ")");
            if(data.error_code == 0){
            jQuery("#schema_org_mapping_row_"+index).remove();
            var rowCount = jQuery('#schema_org_mapping_table tr').length;

            if(rowCount <= 1){
                jQuery('#schema_org_mapping_table').after("<h4> No Entities have currently been mapped.</h4><hr>");
                jQuery('#schema_org_mapping_table').remove();
            }
                jQuery("#content").prepend('<div class="messages status">Removing mapping successful</div>');
            }
            else {
                jQuery("#content").prepend('<div class="messages error">Problem occurred while removing mapping</div>');
            }
        });

    })
})

