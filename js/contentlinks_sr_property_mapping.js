jQuery(document).ready(function($){
    $('.mapping_remove').click(function(){
        var index = $(this).attr('id').replace('button-','');
        $.ajax({
            type: 'POST',
            url: '/contentlinks/remove_sr_property_mapping',
            data: { prop_index: index},
            async: true

        }).done(function (data) {
            data = eval("(" + data + ")");
            if(data.error_code == 0){
                $('tr#row-'+index).remove();
                $("#content").prepend('<div class="messages status">Removing mapping successful</div>');
            }
            else {
                $("#content").prepend('<div class="messages error">Problem occurred while removing mapping</div>');
            }
        });
    })

})
