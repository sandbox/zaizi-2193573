/**
 * Created with JetBrains PhpStorm.
 * User: dphilips
 * Date: 10/30/13
 * Time: 3:02 PM
 * To change this template use File | Settings | File Templates.
 */
jQuery(document).ready(function() {
    var f = jQuery.farbtastic('#colorpicker');
    var p = jQuery('#colorpicker');
    var selected;
    jQuery('.colorwell')
        .each(function () { f.linkTo(this); jQuery(this); })
        .focus(function() {
            if (selected) {
                jQuery(selected).removeClass('colorwell-selected');
            }
            f.linkTo(this);
            p.css('opacity', 1);
            jQuery(selected = this).addClass('colorwell-selected');
        });
});
