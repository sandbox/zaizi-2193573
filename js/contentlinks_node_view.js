jQuery(document).ready(function($){
    function create_properties_popup(entity, element_class, hidden) {
        if(hidden) {
            display = 'display:none';
            element = 'span'
        } else {
            display = 'display:block';
            element = 'div';
        }
        var popup_html = '<'+element+' class="popup-'+element_class+'" style="width: 100%; float:left;margin-bottom: 2px;'+display+'">';
        jQuery.each(entity.properties, function(key, val){
            if(val != '' && val != "") {
                if(hidden) {
                    itemprop = 'itemprop="'+key+'"';
                } else {
                    itemprop = '';
                }
                if(key == 'image') {
                    popup_html += '<'+element+' style="margin-bottom: 2px;"><img '+itemprop+' style="width: 100px;" src="' + val + '"/></div>';
                } else if(key == 'url') {
                    popup_html += '<'+element+' style="margin-bottom: 2px;"> <span style="font-weight: bold;text-transform: capitalize;">'+key+':</span> <a '+itemprop+' target="_blank" href="' + val + '">' + val + '</a></'+element+'>';
                } else if(key == 'description') {
                    popup_html += '<'+element+' style="width: 100%; float:left"> <span style="font-weight: bold;;text-transform: capitalize;">'+key+':</span> <p style="border:1px solid #EEEEEE;float: left;height: 100px;overflow: scroll;padding: 10px;white-space: normal;width: 80%;" '+itemprop+'>' + val + '</p></'+element+'>';
                } else {
                    popup_html += '<'+element+'  style="margin-bottom: 2px;"> <span style="font-weight: bold;text-transform: capitalize;">'+key+':</span> <span '+itemprop+'>' + val + '</span></'+element+'>';
                }
            }

        });
        popup_html +='</'+element+'>';

        return popup_html;
    }
    var entities = Drupal.settings.contentlinks.enhanced_entities;
    $('.contentlinks_enhanced').each(function(){
        //get the field arguments
        var node_div_id = $(this).closest('.node').attr('id');
        var class_array = $(this).attr('class').split(' ');
        var field_array = '';

        for(var i=0; i< class_array.length; i++) {
            if(class_array[i].indexOf('field-') != -1) {
                field_array = class_array[i].replace('field-','').split('-');
                break;
            }
        }
        var field_name = field_array[0];
        var field_index = field_array[1];
        var field_index_property = field_array[2];

        var link = $(this).find('a[class*="link-"]');
        if(link.length > 0) {
            var link_class_array = link.attr('class').split(' '); // split the classes by space
            var entity_selection_array = '';
            var entity_class = '';
            for(var i=0; i< link_class_array.length; i++) {
                if(link_class_array[i].indexOf('select-') != -1) {
                    entity_selection_array = link_class_array[i].replace('select-','').split('-');
                    entity_class = link_class_array[i].replace('select-','')
                    break;
                }
            }

            var entity = entities[node_div_id][field_name][field_index][field_index_property].entities[entity_selection_array[0]][entity_selection_array[1]];
            $(this).CreateBubblePopup({
                position : 'left',
                align	 : 'center',
                innerHtml: create_properties_popup(entity, entity_class, false),
                innerHtmlStyle: {
                    color:'#000000',
                    'text-align':'left',
                    width: '400px',
                    padding: '10px'
                },
                themeName: 'grey',
                themePath: Drupal.settings.basePath + module_path+'/themes/jquery-bubble-popup-themes'

            });
        }

    })
});