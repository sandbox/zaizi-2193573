jQuery(document).ready(function ($) {
    $('fieldset[id$=fieldset] input[type=checkbox].parent-type-checkbox').click(function () {
        if (!$(this).is(":checked")) {
            $(this).parent().siblings('fieldset[id$=fieldset-fields]').find('input[type=checkbox]').each(function () {
                $(this).attr('checked', false);
            });
        }
    });

});