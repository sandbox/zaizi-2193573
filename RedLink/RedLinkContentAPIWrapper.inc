<?php

/**
 * @file
 * This file defines the wrapper class around the RedLink REST API
 * (web scripts) for content management.
 */
class RedLinkContentAPIWrapper extends AbstractRedLinkAPIWrapper
{

    const REDLINK_RESPONSE_FORMAT = 'xml';
    const REDLINK_RESPONSE_FORMAT_ARG_NAME = 'format';

    /**
     * Enhance Content Field service.
     *
     *
     * @param string $field_value
     *   The text value of the filed
     *
     * @return stdClass
     *   Response from RedLink.
     *
     */
    public function enhanceContentField($field_value)
    {
        $service_endpoint = "live";
        $caller = 'RedLinkContentAPIWrapper->enhanceContentField';

        $q_array = array(
            self::REDLINK_RESPONSE_FORMAT_ARG_NAME => self::REDLINK_RESPONSE_FORMAT

        );
        $data = array(
            "bodyParam" => $field_value
        );

        $url = $service_endpoint . "?" . drupal_http_build_query($q_array);
        return $this->service->callServiceEndpoint($caller, $url, 'POST', $data);
    }

}
