<?php

/**
 * @file
 * This file defines an abstract class containing common methods/members for
 * the wrapper classes around the RedLink REST API.
 */

abstract class AbstractRedLinkAPIWrapper
{
    protected $service;

    /**
     * Constructor, sets the $service.
     *
     * @param RedLinkServiceInterface $service
     *   The service object to use for doing requests.
     */
    public function __construct(RedLinkServiceInterface $service)
    {
        $this->service = $service;
    }
}
