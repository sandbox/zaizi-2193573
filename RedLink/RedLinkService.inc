<?php

/**
 * @file
 * This file defines the RedLink service class which can be used for
 * making http request via curl.
 */
interface RedLinkServiceInterface
{

    /**
     * Call RedLink service endpoint.
     *
     * @abstract
     *
     * @param string $caller
     *   Name of the calling service.
     * @param string $service_endpoint
     *   Path of the service endpoint to call.
     * @param string $method
     *   HTTP request method used for the call.
     * @param null|array $data
     *   Data array to use in the request.
     * @param array $curl_options_override
     *   Array of curl options, to override defaults with.
     *
     * @return null|stdClass
     *   Response of the HTTP request.
     */
    public function callServiceEndpoint($caller, $service_endpoint, $method = 'POST', $data = NULL);


    /**
     * Basic validate RedLink tocken.
     *
     * @abstract
     *
     * @param string $api_tocken
     *   RedLink api tocken to be used for authentication.
     *
     * @return bool
     *   TRUE if validation successful, FALSE otherwise.
     */
    public function validateTocken($api_tocken);
}

class RedLinkService implements RedLinkServiceInterface
{

    protected $scheme;
    protected $host;
    protected $port;
    protected $api_endpoint;
    protected $api_tocken;
    protected $analysis_chain;

    const REDLINK_API_KEY_ARGUMENT_NAME = 'api_key';

    /**
     * Constructor, sets hostname and port number.
     *
     * @param string $host
     *   Hostname of RedLink.
     * @param string $port
     *   Port of RedLink.
     * @param string $name
     *   Master user name.
     * @param string $pass
     *   Master user password.
     */
    public function __construct($scheme, $host, $port, $api_endpoint, $analysis_chain, $api_tocken)
    {
        $this->scheme = $scheme;
        $this->host = $host;
        $this->port = $port;
        $this->api_endpoint = $api_endpoint;
        $this->api_tocken = $api_tocken;
        $this->analysis_chain = $analysis_chain;
    }

    public function getScheme()
    {
        return $this->scheme;
    }

    public function setScheme($scheme)
    {
        $this->scheme = $scheme;
    }

    public function getHost()
    {
        return $this->host;
    }

    public function setHost($host)
    {
        $this->host = $host;
    }

    public function getPort()
    {
        return $this->port;
    }

    public function setPort($port)
    {
        $this->port = $port;
    }

    public function getApiEndpoint()
    {
        return $this->api_endpoint;
    }

    public function setApiEndpoint($api_endpoint)
    {
        $this->api_endpoint = $api_endpoint;
    }

    public function getApiTocken()
    {
        return $this->api_tocken;
    }

    public function setApiTocken($api_tocken)
    {
        $this->api_tocken = $api_tocken;
    }

    public function getAnalysisChain()
    {
      return $this->analysis_chain;
    }

    public function setAnalysisChain($analysis_chain)
    {
      $this->analysis_chain = $analysis_chain;
    }


    /**
     * Get the default curl options array for a POST request.
     *
     * @param string $url
     *   URL of service endpoint to call.
     * @param array $data
     *   Data to use in POST request.
     *
     * @return array
     *   POST request specific curl options.
     */
    protected function GetCurlPostOptions($url, $data)
    {

        $url = $this->appendRedLinkTockenToQueryString($url);
        $ret = array(
            CURLOPT_URL => $url,
            CURLOPT_FAILONERROR => TRUE, // FALSE if in debug mode
            CURLOPT_RETURNTRANSFER => TRUE,
            CURLOPT_TIMEOUT => 4,
            CURLOPT_HTTPHEADER => array('Content-Type:application/json', 'Expect:'),
            CURLOPT_POST => TRUE,
            CURLOPT_POSTFIELDS => $data,
        );
        return $ret;
    }

    /**
     * Get the default curl options array for a GET request.
     *
     * @param string $url
     *   URL of service endpoint to call.
     *
     * @return array
     *   GET request specific curl options.
     */
    protected function GetCurlGetOptions($url)
    {
        $url = $this->appendRedLinkTockenToQueryString($url);
        $ret = array(
            CURLOPT_URL => $url,
            CURLOPT_FAILONERROR => TRUE,
            CURLOPT_RETURNTRANSFER => TRUE,
            CURLOPT_BINARYTRANSFER => 1,
            CURLOPT_TIMEOUT => 3,
            CURLOPT_HTTPHEADER => array('Content-Type:application/json'),
        );
        return $ret;
    }

    /**
     * Get the default curl options array for a DELETE request.
     *
     * @param string $url
     *   URL of service endpoint to call.
     *
     * @return array
     *   DELETE request specific curl options.
     */
    protected function GetCurlDeleteOptions($url)
    {
        $url = $this->appendRedLinkTockenToQueryString($url);
        $ret = array(
            CURLOPT_URL => $url,
            CURLOPT_FAILONERROR => TRUE,
            CURLOPT_RETURNTRANSFER => TRUE,
            CURLOPT_TIMEOUT => 3,
            CURLOPT_HTTPHEADER => array('Content-Type:application/json'),
            CURLOPT_CUSTOMREQUEST => 'DELETE',
        );
        return $ret;
    }

    /**
     * Constructs the full URL for a $service_endpoint.
     *
     * @param string $service_endpoint
     *   Path of the service endpoint.
     *
     * @return string
     *   Full URL of service endpoint, including scheme, hostname, port number.
     */
    public function constructServiceUrl($service_endpoint = null)
    {
        if (!is_null($service_endpoint)) {
            return "{$this->scheme}://{$this->host}:{$this->port}/{$this->api_endpoint}/{$service_endpoint}";
        } else {
            return "{$this->scheme}://{$this->host}:{$this->port}/{$this->api_endpoint}";
        }

    }

    /**
     * Appends $this->api_tocken as query string to $url.
     *
     * @param string $url
     *   URL to append to.
     *
     * @return string
     *   URL with $this->api_tocken appended as query string.
     */
    protected function appendRedLinkTockenToQueryString($url)
    {
        $api_key_argument_name = self::REDLINK_API_KEY_ARGUMENT_NAME;
        $api_key_query_string = "{$api_key_argument_name}={$this->api_tocken}";
        $url .= (strpos($url, '?') !== FALSE ? '&' : '?') . $api_key_query_string;
        return $url;
    }

    /**
     * Wrapper function around a curl request.
     *
     * @param string $caller
     *   Name of the calling service.
     * @param string $service_endpoint
     *   Path of the service endpoint to call.
     * @param string $method
     *   HTTP request method used for the call.
     * @param null|array $data
     *   Data array to use in the request.
     * @param array $curl_options_override
     *   Array of curl options, to override defaults with.
     *
     * @return null|stdClass
     *   Response object with response, error, info fields.
     */
    protected function curlHTTPRequest($caller, $service_endpoint, $method, $data = NULL, $curl_options_override = array())
    {

        $curl_handle = curl_init();

        $url = $this->constructServiceUrl($service_endpoint);
        switch ($method) {
            case 'POST':
                $curl_options = $this->GetCurlPostOptions($url, $data);
                break;

            case 'GET':
                $curl_options = $this->GetCurlGetOptions($url);
                break;

            case 'DELETE':
                $curl_options = $this->GetCurlDeleteOptions($url);
                break;

            default:
                return NULL;
        }

        if (is_array($curl_options_override) && !empty($curl_options_override)) {
            foreach ($curl_options_override as $option => $value) {
                if (isset($curl_options[$option])) {
                    switch ($option) {
                        case CURLOPT_HTTPHEADER:
                            $curl_options[$option] = array_merge($curl_options[$option], $value);
                            break;

                        default:
                            // @TODO: Implement possibility to override default curl options.
                            break;
                    }
                } else {
                    $curl_options[$option] = $value;
                }
            }
        }

        curl_setopt_array($curl_handle, $curl_options);

        curl_setopt($curl_handle, CURLOPT_SSL_VERIFYPEER, FALSE);
        curl_setopt($curl_handle, CURLOPT_SSL_VERIFYHOST, FALSE);
        curl_setopt($curl_handle, CURLOPT_VERBOSE, TRUE);
        curl_setopt($curl_handle, CURLOPT_STDERR, fopen(realpath(drupal_realpath('public://')) . "/curl_{$caller}.txt", 'w'));
        curl_setopt($curl_handle, CURLOPT_TIMEOUT, 300);

        $ret = new stdClass();
        $ret->response = curl_exec($curl_handle);
        $ret->error = curl_error($curl_handle);
        $ret->info = curl_getinfo($curl_handle);
        $ret->caller = $caller;

        curl_close($curl_handle);
        return $ret;
    }


    /**
     * Basic validate RedLink tocken.
     *
     * @abstract
     *
     * @param string $api_tocken
     *   RedLink api tocken to be used for authentication.
     *
     * @return bool
     *   TRUE if validation successful, FALSE otherwise.
     */
    public function validateTocken($api_tocken)
    {
        /**
         * @TODO Check if tocken is valid and return suitable value.
         */
    }


    /**
     * Wrapper function that calls a service endpoint via curlHTTPRequest().
     *
     * @param string $caller
     *   Name of the calling service.
     * @param string $service_endpoint
     *   Path of the service endpoint to call.
     * @param string $method
     *   HTTP request method used for the call.
     * @param null|array $data
     *   Data array to use in the request.
     * @param array $curl_options_override
     *   Array of curl options, to override defaults with.
     *
     * @return null|stdClass
     *   Response of the HTTP request.
     *
     * @throws RedLinkServiceException
     */
    public function callServiceEndpoint($caller, $service_endpoint, $method = 'POST', $data = NULL, $curl_options_override = array())
    {
        $response = $this->curlHTTPRequest($caller, $service_endpoint, $method, $data, $curl_options_override);
        if ($response->info['http_code'] === 200) {
            return $response->response;
        } else {
            throw new RedLinkServiceException("Error happened while calling the {$caller} service of RedLink.", 500, $response);
        }
    }

}

/**
 * A exception thrown by the RedLink Service when something goes wrong.
 */
class RedLinkServiceException extends Exception
{

    protected $data;

    /**
     * Constructor for the RedLinkServiceException.
     *
     * @param string $message
     *   Error message.
     * @param int $code
     *   Optional. Error code. This often maps to the HTTP status codes. Defaults
     *   to 0.
     * @param mixed $data
     *   Information that can be used by the server to return information about
     *   the error.
     */
    public function __construct($message, $code = 0, $data = NULL)
    {
        parent::__construct($message, $code);

        $this->data = $data;
    }

    /**
     * Returns the data associated with the exception.
     *
     * @return mixed
     *   Exception data.
     */
    public function getData()
    {
        return $this->data;
    }

}
