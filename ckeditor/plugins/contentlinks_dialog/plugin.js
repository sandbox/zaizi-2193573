/**
 * @author
 * Niraj Meegama
 * Dishan Philips
 * Antonio David Perez Morales
 */
var selected_entities = {};
var current_text_annotation_index = 0;
var current_entity_selection_value = 0;
var current_schema_further_enhance_checked = false;
var current_schema_further_enhance_entity_value = 0;
var schemaORGEntities = [];
var supportedEntities = ["Person", "Place", "Organization", "Other", "Empty"];

(function ($) {
    jQuery('#edit-submit').click(function(e){
        var selEntities = "";
        for(var val in selected_entities) {
            if(selected_entities[val] != undefined)
                selEntities += selected_entities[val]+" ";
        }
        selEntities = selEntities.replace(/\s*$/g, '');
        var $input = jQuery("<input/>").attr({type: 'hidden', value: selEntities, name: 'selected_entities'});
        jQuery(this).closest('form').append($input);

    });

    function add_selected_entity_uri_to_field(field_name, field_index, field_index_property, entity) {
        entity.uri = entity.uri.replace(',','');
        var field_selected_entities = $('input[name^="contentlinks_selected_entities"]');
        var field_selected_entities_val = field_selected_entities.val();
        if(field_selected_entities_val == '') {
            field_selected_entities.val(field_name +'-'+field_index + '-' + field_index_property + '->'+entity.uri);
        } else {
            var current_val = field_name +'-'+field_index + '-' + field_index_property + '->'+entity.uri;
            if (!is_current_value_already_existing(current_val, field_selected_entities_val)) {
                field_selected_entities.val(field_selected_entities_val + "," + field_name +'-'+field_index + '-' + field_index_property + '->'+entity.uri);
            }
        }
    }

    /**
     * A function to add values to the tags field in a comma separated list
     * @param jquery_element
     */
    function auto_tag_keywords(current_val, parent) {
        if(parent != 'Empty') {
            var field_tags = $('input[name^="field_tags"]');
            var field_tags_val = field_tags.val();
            if (field_tags_val == '') {
                field_tags.val(current_val);
                link_tags_to_parent(current_val, parent);
            } else {
                if (!is_current_value_already_existing(current_val, field_tags_val)) {
                    field_tags.val(field_tags_val + "," + current_val);
                    link_tags_to_parent(current_val, parent);
                }
            }
        }



    }

    /**
     * A function to add tags with a hierarchy
     * @param tag
     * @param parent
     */
    function link_tags_to_parent(tag, parent) {
        if(parent == 'Organisation') {
            parent = 'Organization';
        }
        var field_tags = $('input[name^="contentlinks_tags"]');
        var field_tags_val = field_tags.val();
        var current_val = parent + '->' + tag;
        if (field_tags_val == '') {
            field_tags.val(current_val);
        } else {
            if (!is_current_value_already_existing(current_val, field_tags_val)) {
                field_tags.val(field_tags_val + "," + current_val);
            }
        }
    }

    /**
     * A function to check if the current value is already a tag
     * @param current_val
     * @param field_tags_val
     * @returns {boolean}
     */
    function is_current_value_already_existing(current_val, field_tags_val) {
        field_tags_val = field_tags_val.replace(/, /g, ',');
        var field_tags_array = field_tags_val.split(',');
        if (jQuery.inArray(current_val, field_tags_array) !== -1) {
            return true; // the value is already on the field
        } else {
            return false;
        }
    }
    /**
     *A function to set the html element inside the dialog
     *
     */
    function set_dialog_html(entity) {
        var desc_html = (entity.description == '') ? '' : '<div style="width: 100%; float:left"> <span style="font-weight: bold;">Description:</span> <p style="border:1px solid #EEEEEE;float: left;height: 100px;overflow: scroll;padding: 10px;white-space: normal;width: 90%;">' + entity.description + '</p></div>';
        var html_str = '<div style="margin:10px;border:1px solid #cccccc;padding: 10px; float:left;">' +
                            '<div style="width: 100%; float:left;margin-bottom: 2px;">'+
                                '<div style="width:70%;float: left;margin-bottom: 2px;">' +
                                    '<div style="margin-bottom: 2px;"> <span style="font-weight: bold;">Label:</span> ' + entity.label + '</div>' +
                                    '<div style="margin-bottom: 2px;"> <span style="font-weight: bold;">Type:</span> ' + entity.type + ((!entity.db_type)? '' : ' ( '+entity.db_type + ' )') + '</div>' +
                                    '<div style="margin-bottom: 2px;"> <span style="font-weight: bold;">URI:</span> <a style="float:left;overflow: hidden;text-overflow: ellipsis;width: 250px" target="_blank" href="' + entity.uri + '">' + entity.uri + '</a></div>' +
                                '</div>';
        if(entity.thumb != '' && entity.thumb != "" && entity.thumb != null && entity.thumb != undefined) {
            html_str += '<div style="width: 30%;margin-bottom: 2px;"> ' +
                            '<img style="width: 100px;" src="' + entity.thumb + '"/>' +
                        '</div>';
        }
        html_str += '</div>' +
                            desc_html +
                    '</div>';
        return html_str;
    }

    function create_properties_html(entity) {
        var popup_html = '';
        jQuery.each(entity.properties, function(key, val){
            if(val != '' && val != "" && key!='url') {
                if(key == 'image') {
                    popup_html += '<img class="contentlinks-properties" style="display:none" itemprop="'+key+'" style="width: 100px;" src="' + val + '"/>';
//                } else if(key == 'url') {
//                    popup_html += '<a class="contentlinks-properties" style="display:none" itemprop="'+key+'" target="_blank" href="' + val + '">' + val + '</a>';
                } else if(key == 'description') {
                    popup_html += '<p class="contentlinks-properties" style="display:none" itemprop="'+key+'">' + val + '</p>';
                } else {
                    popup_html += ' <span class="contentlinks-properties" style="display:none" itemprop="'+key+'">' + val + '</span>';
                }
            }

        });

        return popup_html;
    }

    function restuctureEditorAndDialog(dialog, isShown) {
        if(isShown) {
            console.log($("#cke_"+dialog.getParentEditor().name).parent().position().top);
            $('#culrly_bob').remove();
            $("#cke_"+dialog.getParentEditor().name).parent().css({width : '100%', float: 'left'});
            $("#cke_"+dialog.getParentEditor().name).css({width : '69%', float: 'left'});
            if($('.dummy-div').length == 0){
                $("#cke_"+dialog.getParentEditor().name).after('<div class="dummy-div" style="width:29%; float:left;height:'+ $("#cke_"+dialog.getParentEditor().name).height()+'px;"></div>');
            }

            if($('#culrly_bob').length == 0) {
                $("#cke_"+dialog.getParentEditor().name).after('<img id="culrly_bob" style="width: 1%; float:left; margin-top: 6%;" src="'+dialog.getParentEditor().plugins.contentlinks_dialog.path + 'icons/arrow.png"/>');
            }

            $(".cke_dialog").attr('style','');
            $(".cke_dialog").height($("#cke_"+dialog.getParentEditor().name).height());
            $(".cke_dialog").css({top: parseInt($("#cke_"+dialog.getParentEditor().name).position().top) + parseInt($("#cke_"+dialog.getParentEditor().name).parent().position().top) - 24, right: 20, position:'absolute', width : '29%', float: 'left', overflow: 'hidden'});
            $(".cke_dialog_contents").css({width : '100%'});
        } else {
            $('.dummy-div').remove();
            $('#culrly_bob').remove();
            $("#cke_"+dialog.getParentEditor().name).removeAttr('style');

        }

    }

    CKEDITOR.plugins.add('contentlinks_dialog', {
        init: function (editor) {
            var field = editor.element.getNameAtt();
            var field_name = field.replace(/\s*\[.*?\]\s*/g, '');
            if (Drupal.settings.contentlinks == undefined) return;
            if(Drupal.settings.contentlinks.fields_to_enhance == undefined || jQuery.inArray(field_name, Drupal.settings.contentlinks.fields_to_enhance) === -1) return;


            if (Drupal.settings.contentlinks.enhanced_entities != undefined) {
                CKEDITOR.contentlinks = Drupal.settings.contentlinks.enhanced_entities;
            }
            if(Drupal.settings.contentlinks.schema_org_properties != undefined) {
                schemaORGEntities = Drupal.settings.contentlinks.schema_org_properties;
            }

//            editor.ui.addButton('my_dialog_button', {
//                label: 'Open Dialog', //this is the tooltip text for the button
//                icon: this.path + 'icons/open.png',
//                command: 'my_dialog_open'
//            });
//            editor.addCommand('my_dialog_open',new CKEDITOR.dialogCommand( 'mydialog' ));
            CKEDITOR.dialog.add('contentlinks_dialog', function (api) {

                // CKEDITOR.dialog.definition
                var dialogDefinition =
                {
                    title: 'Select entities for selected text annotations',
                    minWidth: 100,
                    minHeight: 0,
                    contents: [
                        {
                            id: 'tab',
                            label: 'Select Entities',
                            title: 'Select Entities',
                            expand: true,
                            padding: 0,
                            elements: [
                                {
                                    type: 'select',
                                    id: 'contentlinks_entities',
                                    label: 'Select Entities',
                                    items: [],
                                    onChange: function (e) {
                                        restuctureEditorAndDialog(this.getDialog(), true);
                                        var field = this.getDialog().getParentEditor().element.getNameAtt(); // get the current field Eg: body[und][0][value]
                                        var field_name = field.replace(/\s*\[.*?\]\s*/g, ''); // get the field name only Eg: body[und][0][value] => body
                                        var field_index = field.replace(/^.*?\[(\D*)\]/g, '').replace(/\[\D*\]/g, '').replace(/[\[]|[\]]/g, ''); //Get the index of the field Eg: body[und][0][value] => 0
                                        var field_index_property = field.replace(/^[^\[]+\[[^\]]+\]\[(\d+)\]/g, '').replace(/[\[]|[\]]/g, ''); //Get the field property Eg: body[und][0][value] => value
                                        current_entity_selection_value = this.getValue();
                                        var html_element = this.getDialog().getContentElement('tab', 'dialog_html');
                                        if (current_entity_selection_value != '') {
                                            //get the current entity object by index
                                            var entity_value_array = current_entity_selection_value.split('-');
                                            var entity_index = entity_value_array[1];
                                            var entity = CKEDITOR.contentlinks[field_name][field_index][field_index_property].entities[current_text_annotation_index][entity_index];

                                            var html_str = set_dialog_html(entity);
                                            html_element.getElement().setHtml(html_str);
                                        } else {
                                            html_element.getElement().setHtml('');
                                        }

                                    }


                                },
                                {
                                    id: 'dialog_html',
                                    type: 'html',
                                    html: ''
                                },
                                {
                                    type: 'checkbox',
                                    id: 'contentlinks_enhance_further',
                                    label: 'Enhance Further',
                                    'default': '',
                                    onChange: function( api ) {
                                        current_schema_further_enhance_checked = this.getValue();
                                        var dialogTabSchemaORGSelect = this.getDialog().getContentElement('tab', 'schemaorg_further_enhance_entities');
                                        if(current_schema_further_enhance_checked) {
                                            dialogTabSchemaORGSelect.enable();
                                            $('#'+dialogTabSchemaORGSelect.domId).show();
                                        } else {
                                            dialogTabSchemaORGSelect.disable();
                                            $('#'+dialogTabSchemaORGSelect.domId).hide();
                                        }
                                    }
                                },
                                {
                                    type: 'select',
                                    id: 'schemaorg_further_enhance_entities',
                                    label: 'Select Item Type',
                                    items: [],

                                    onChange: function( api ) {
                                        current_schema_further_enhance_entity_value = this.getValue();
                                    }
                                }
                            ]
                        }
                    ],
                    buttons: [
                        {
                            type: 'button',
                            id: 'accept',
                            label: 'Accept',
                            title: 'Accept',
                            className: 'cke_dialog_ui_button_ok',
                            onClick: function () {
                                var field = this.getDialog().getParentEditor().element.getNameAtt(); // get the current field Eg: body[und][0][value]
                                var field_name = field.replace(/\s*\[.*?\]\s*/g, ''); // get the field name only Eg: body[und][0][value] => body
                                var field_index = field.replace(/^.*?\[(\D*)\]/g, '').replace(/\[\D*\]/g, '').replace(/[\[]|[\]]/g, ''); //Get the index of the field Eg: body[und][0][value] => 0
                                var field_index_property = field.replace(/^[^\[]+\[[^\]]+\]\[(\d+)\]/g, '').replace(/[\[]|[\]]/g, ''); //Get the field property Eg: body[und][0][value] => value
                                this.getDialog().hide();

                                var jquery_element = $(this.getDialog().getParentEditor().window.getFrame().$).contents().find('.text-' + current_text_annotation_index);

                                jquery_element.find('br').remove();
                                if (current_entity_selection_value != 0) {
                                    //removing the inner link
                                    if (jquery_element.find('a[class^="link"]').length) {
                                        jquery_element.html(jquery_element.find('a[class^="link"]').html());
                                    }
                                    // removing all enhancements
                                    if(jquery_element.parent().is('.contentlinks_enhanced')) {
                                        jquery_element.unwrap();
                                        jquery_element.removeAttr('itemprop');
                                        jquery_element.find('.contentlinks-properties').remove();

                                    }

                                    var entity_value_array = current_entity_selection_value.split('-');
                                    var entity_index = entity_value_array[1];
                                    var entity = CKEDITOR.contentlinks[field_name][field_index][field_index_property].entities[current_text_annotation_index][entity_index];

                                    var tag = jquery_element.html().toString().trim().replace(/,/g,'');
                                    auto_tag_keywords(tag, entity.type);

                                    $(supportedEntities).each(function(key,value){
                                        jquery_element.removeClass('contentlinks-entity-'+value);
                                    });

                                    jquery_element.addClass('contentlinks-entity-'+entity.valid_type);

                                    selected_entities[current_entity_selection_value] = entity.uri;
                                    var field_class = 'field-'+field_name+'-'+field_index+'-'+field_index_property
                                    if(current_schema_further_enhance_checked){
                                        jquery_element.wrap('<div itemscope style="display:inline;" class="contentlinks_enhanced contentlinks_further_enhanced '+field_class+'" itemtype="'+current_schema_further_enhance_entity_value+'"/>');
                                        jquery_element.attr('itemprop', 'name');
                                    } else if(entity.db_type != false) {
                                        jquery_element.wrap('<div itemscope style="display:inline;" itemtype="'+entity.db_type+'" class="contentlinks_enhanced '+field_class+'" />');
                                        jquery_element.attr('itemprop', 'name');

                                    } else {
                                        jquery_element.wrap('<div itemscope style="display:inline;" class="contentlinks_enhanced '+field_class+'" />');
                                        jquery_element.attr('itemprop', 'name');
                                    }
                                    //Change the background color according to the type selected
                                    jquery_element.html('<a itemprop="url" class="link-'+current_text_annotation_index+' select-'+ current_entity_selection_value + '" target="_blank" href="'+entity.uri+'" >'+jquery_element.html()+'</a>');

                                    //add the selcted entiyt urn to the field
                                    add_selected_entity_uri_to_field(field_name, field_index, field_index_property, entity);

                                    //appending the properties to the textAnnotation
                                    var popup_html = create_properties_html(entity);
                                    jquery_element.append(popup_html);

                                    current_text_annotation_index = 0;
                                    current_entity_selection_value = 0;
                                } else {
                                    jquery_element.css('background', 'none');
                                }
                                restuctureEditorAndDialog(this.getDialog(), false);
                            }
                        },
                        {
                            type: 'button',
                            id: 'decline',
                            label: 'Decline',
                            title: 'Decline',
                            className: 'cke_dialog_ui_button_cancel',
                            onClick: function () {
                                this.getDialog().hide();
                                var jquery_element = $(this.getDialog().getParentEditor().window.getFrame().$).contents().find('.text-' + current_text_annotation_index);
                                //removing the inner link
                                if (jquery_element.find('a[class^="link"]').length) {
                                    jquery_element.html(jquery_element.find('a[class^="link"]').html());
                                }
                                // removing all enhancements
                                if(jquery_element.parent().is('.contentlinks_enhanced')) {
                                    jquery_element.unwrap();
                                    jquery_element.removeAttr('itemprop');
                                    jquery_element.find('.contentlinks-properties').remove();

                                }
                                jquery_element.wrapInner('<div class="contentlinks-declined" style="display:inline-block;"/>');
                                jquery_element.find('div.contentlinks-declined').unwrap();
                                $(this.getDialog().getParentEditor().window.getFrame().$).contents().find('.entity-wrapper-' + current_text_annotation_index).remove();
                                current_text_annotation_index = 0;
                                current_entity_selection_value = 0;
                                restuctureEditorAndDialog(this.getDialog(), false);
                            }


                        }

                    ],
                    onHide: function (e) {
                        restuctureEditorAndDialog(this, false);
                    },
                    onShow: function (e) {

                        var field = this.getParentEditor().element.getNameAtt(); // get the current field Eg: body[und][0][value]
                        var field_name = field.replace(/\s*\[.*?\]\s*/g, ''); // get the field name only Eg: body[und][0][value] => body
                        var field_index = field.replace(/^.*?\[(\D*)\]/g, '').replace(/\[\D*\]/g, '').replace(/[\[]|[\]]/g, ''); //Get the index of the field Eg: body[und][0][value] => 0
                        var field_index_property = field.replace(/^[^\[]+\[[^\]]+\]\[(\d+)\]/g, '').replace(/[\[]|[\]]/g, ''); //Get the field property Eg: body[und][0][value] => value
                        $('.cke_dialog_background_cover').remove(); // remove the mask of the dialog
                        var dialogTabSelect = this.getContentElement('tab', 'contentlinks_entities'); // getting the entity selection dialog

                        var html_element = this.getContentElement('tab', 'dialog_html'); // html box for the dialog
                        html_element.getElement().setHtml(''); // Clear current html

                        var entities = CKEDITOR.contentlinks[field_name][field_index][field_index_property].entities; // get the entities for the current field
                        dialogTabSelect.clear(); // Clear the main entity select to fill up with new entities
                        for (var val in entities[current_text_annotation_index]) {
                            //adding new entities
                            dialogTabSelect.add(entities[current_text_annotation_index][val].label, current_text_annotation_index + "-" + val);
                        }
                        // The current_text_annotation_index should be set up in the click event of the element
                        if (current_text_annotation_index != 0) {
                            var current_text_annotation_element = $(this.getParentEditor().window.getFrame().$).contents().find('.text-' + current_text_annotation_index);
                            // check if the textAnnotation is already enhanced
                            if (current_text_annotation_element.find('a.link-' + current_text_annotation_index).length > 0) {
                                var link_class_array = current_text_annotation_element.find('a.link-' + current_text_annotation_index).attr('class').split(' ');
                                var link_select_class_array = link_class_array[1].split('-');
                                var select_value = link_select_class_array[1] + '-' + link_select_class_array[2];
                                dialogTabSelect.setValue(select_value);

                            }
                        }

                        current_entity_selection_value = dialogTabSelect.getValue(); // set up the global var current_entity_selection_value

                        if (current_entity_selection_value != '') {
                            var entity_value_array = current_entity_selection_value.split('-');
                            var entity_index = entity_value_array[1];
                            var entity = entities[current_text_annotation_index][entity_index];

                            // adding the schema.org entities for further enhancements
                            var dialogTabEnhanceFurtherCheckbox = this.getContentElement('tab', 'contentlinks_enhance_further');
                            $("#"+dialogTabEnhanceFurtherCheckbox.domId).hide();
                            var dialogTabSchemaORGSelect = this.getContentElement('tab', 'schemaorg_further_enhance_entities');
                            dialogTabSchemaORGSelect.disable(); // Disable the further enhancing select box
                            $('#'+dialogTabSchemaORGSelect.domId).hide(); // hide the element

                            dialogTabSchemaORGSelect.clear();
                            if(schemaORGEntities.types != undefined){
                                var found =  false;

                                var furtherEnhancedEntity = $(this.getParentEditor().window.getFrame().$).contents().find('.text-' + current_text_annotation_index);
                                furtherEnhancedEntity = furtherEnhancedEntity.find("div.contentlinks_further_enhanced");
                                furtherEnhancedEntity = furtherEnhancedEntity.attr("itemtype");

                                jQuery.each(schemaORGEntities.types, function(i, val) {
                                    if((val.ancestors.indexOf(entity.type) >= 0) ) {
                                        dialogTabSchemaORGSelect.add([val.label],val.url);
                                        if(!found) {
                                            found = true;
                                        }
                                    }
                                });
                                current_schema_further_enhance_entity_value = dialogTabSchemaORGSelect.getValue();

                                if(!found) {
                                    current_schema_further_enhance_entity_value = 0;
                                    current_schema_further_enhance_checked = false;
                                }
                                else{
                                    $("#"+dialogTabEnhanceFurtherCheckbox.domId).show();
                                }


                                if(furtherEnhancedEntity != null){
                                    $("#"+dialogTabEnhanceFurtherCheckbox.domId).find("input[type=checkbox]").attr('checked','checked');
                                    dialogTabSchemaORGSelect.setValue(furtherEnhancedEntity);
                                }

                            } else {
                                //of no schema.org service then hide the checkbox
                                $("#"+dialogTabEnhanceFurtherCheckbox.domId).hide();

                            }

                            // setting up the html with the current_entity_selection_value properties
                            var html_str = set_dialog_html(entity);
                            html_element.getElement().setHtml(html_str);
                        } else {
                            html_element.getElement().setHtml('');
                        }
                        restuctureEditorAndDialog(this, true);
                    }

                };

                return dialogDefinition;
            });
            editor.on('contentDom', function (e) {
                $(e.editor.window.getFrame().$).contents().find('.contentlinks-wrapper').click(function () {
                    // setting up the click function
                    var class_array = $(this).attr('class').split(' ');
                    var text_annotation_class = class_array[1].split('-');
                    current_text_annotation_index = text_annotation_class[1];
                    e.editor.openDialog('contentlinks_dialog');
                });

            });
            editor.on('change', function (e) {
                $(e.editor.window.getFrame().$).contents().find('.contentlinks-wrapper').click(function () {
                    // setting up the click function
                    var class_array = $(this).attr('class').split(' ');
                    var text_annotation_class = class_array[1].split('-');
                    current_text_annotation_index = text_annotation_class[1];
                    e.editor.openDialog('contentlinks_dialog');
                });

                $(e.editor.window.getFrame().$).contents().find('.cke_dialog').change(function () {
                    console.log('here reposition')
                });


            });
        }
    });



})(jQuery);