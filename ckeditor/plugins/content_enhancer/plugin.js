/**
 * @author
 * Niraj Meegama
 * Dishan Philips
 * Antonio David Perez Morales
 */

(function ($) {

    CKEDITOR.plugins.add('content_enhancer', {
        init: function (editor) {
            var field = editor.element.getNameAtt();
            var field_name = field.replace(/\s*\[.*?\]\s*/g, '');
            if (Drupal.settings.contentlinks == undefined) return;
            if(Drupal.settings.contentlinks.fields_to_enhance == undefined || jQuery.inArray(field_name, Drupal.settings.contentlinks.fields_to_enhance) === -1) return;
            CKEDITOR.addCss('.contentlinks-wrapper a, .contentlinks_enhanced a, .contentlinks-wrapper, contentlinks_enhanced {cursor: pointer !important;} '+ Drupal.settings.contentlinks.color_codes);
            editor.config.enterMode
            editor.ui.addButton('content_enhancer', {
                label: 'Enhance', //this is the tooltip text for the button
                icon: this.path + 'icons/enhance.png',
                command: 'content_enhancer'
            });

            editor.addCommand('content_enhancer', {

                exec: function () {

                    var content = editor.getData();
                    //here is where we tell CKEditor what to do.
                    /*
                     * Getting rid of the html added by the enhancer
                     * Uncomment once html content is allowed
                     */
                    var content_element = document.createElement('div');
                    $(content_element).addClass('contentlinks-parent').html(content);
                    $(content_element).find('.contentlinks-properties').remove();

                    content = $(content_element).html().replace(/&nbsp;/gi,' ');

                    var field = editor.element.getNameAtt();
                    var field_name = field.replace(/\s*\[.*?\]\s*/g, '');
                    var field_index = field.replace(/^.*?\[(\D*)\]/g, '').replace(/\[\D*\]/g, '').replace(/[\[]|[\]]/g, '');
                    var field_index_property = field.replace(/^[^\[]+\[[^\]]+\]\[(\d+)\]/g, '').replace(/[\[]|[\]]/g, '');


                    //remove all hidden entity wrappers
                    var content_element = document.createElement('div');
                    $(content_element).addClass('contentlinks-parent').html(content);
                    $(content_element).find('[class^="entity-wrapper"]').remove();
                    content = $(content_element).html();
                    Pace.restart();
                    $.ajax({
                        type: 'POST',
                        url: Drupal.settings.basePath + "contentlinks/content_enhance",
                        async: false,
                        data: {
                            content: content,
                            type: $('.node-form').attr('id'),
                            token: $('input[name="contentlinks_token"]').val(),
                            field_machine_name: field_name,
                            field_index: field_index,
                            field_property: field_index_property
                        }

                    }).done(function (data) {
                            if (data) {
                                var field = editor.element.getNameAtt();
                                var field_name = field.replace(/\s*\[.*?\]\s*/g, '');
                                var field_index = field.replace(/^.*?\[(\D*)\]/g, '').replace(/\[\D*\]/g, '').replace(/[\[]|[\]]/g, '');
                                var field_index_property = field.replace(/^[^\[]+\[[^\]]+\]\[(\d+)\]/g, '').replace(/[\[]|[\]]/g, '');
                                var resultObj = eval("(" + data + ")");
                                if (CKEDITOR.contentlinks == undefined) {
                                    CKEDITOR.contentlinks = {};
                                }
                                if (CKEDITOR.contentlinks[field_name] == undefined) {
                                    CKEDITOR.contentlinks[field_name] = {};
                                }
                                if (CKEDITOR.contentlinks[field_name][field_index] == undefined) {
                                    CKEDITOR.contentlinks[field_name][field_index] = {};
                                }
                                CKEDITOR.contentlinks[field_name][field_index][field_index_property] = { entities: resultObj.entities };
                                editor.dataProcessor.writer.setRules( 'div',
                                    {
                                        indent : false,
                                        breakBeforeOpen : true,
                                        breakAfterOpen : false,
                                        breakBeforeClose : false,
                                        breakAfterClose : true
                                    });
                                editor.setData('', function () {
                                    this.insertHtml('<div>'+resultObj.enhanced_content+'</div>','');
                                    $(this.window.getFrame().$).contents().find('head').append('<style>'+resultObj.color_css+'</style>');
                                    $(this.window.getFrame().$).contents()
                                    if ($('#cke_'+this.name+' .cke_button__content_enhancer').parent().find('.legend-image').length == 0) {
                                        $('#cke_'+this.name+' .cke_button__content_enhancer').parent().append(
                                            '<span class="legend-image">' +
                                                '<span class="contentlinks-entity-Person legend-box">&nbsp;&nbsp;&nbsp;&nbsp;</span> Person ' +
                                                '<span class="contentlinks-entity-Place legend-box">&nbsp;&nbsp;&nbsp;&nbsp;</span> Place ' +
                                                '<span class="contentlinks-entity-Organization legend-box">&nbsp;&nbsp;&nbsp;&nbsp;</span> Organization ' +
                                                '<span class="contentlinks-entity-Other legend-box">&nbsp;&nbsp;&nbsp;&nbsp;</span> Other' +
                                            '</span>');
                                    }
                                });
                                Drupal.settings.contentlinks.enhanced = true; // To keep track if content is enhanced.
                            } else {
                                Pace.stop();
                                alert("error");
                            }
                        });
                }
            });
        }
    });
})(jQuery);