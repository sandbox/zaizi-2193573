<?php

// $Id$
/**
 * @file
 * This module provides methods functionality to enrich selected
 * content type fieds with semantic markup using the RedLink API
 *
 *
 */

function contentlinks_requirements($phase) {
    $requirements = array();
  if($phase == 'runtime') {
    // Ensure translations do not break at install time
    $t = get_t();

    $requirements['contentlinks'] = array(
      'title' => $t('RedLinkPHPSDK'),
    );

    if (function_exists('\http_build_url')) {
      $requirements['contentlinks']['value'] = $t('Installed');
      $requirements['contentlinks']['severity'] = REQUIREMENT_OK;
    }
    else {
      error_log('died');
      die;
      $requirements['contentlinks']['value'] = $t('Not Installed');
      $requirements['contentlinks']['severity'] = REQUIREMENT_ERROR;
      $requirements['contentlinks']['description'] = $t('Please install the RedLinkPHPSDK library %url.', array('%url' => 'http://zaizi.com'));
    }
  }


    return $requirements;
}

/**
 * Implement hook_install().
 */
function contentlinks_install()
{
    //Initializing variables
    variable_set('contentlinks_redlink_server_scheme', 'http');
    variable_set('contentlinks_redlink_server_name', 'localhost');
    variable_set('contentlinks_redlink_server_port', '8080');
    variable_set('contentlinks_redlink_server_endpoint', 'api');
    variable_set('contentlinks_redlink_server_analysis_chain', 'anlysis_chain_name');
    variable_set('contentlinks_redlink_server_api_token', 'xxxx');
    variable_set('contentlinks_field_prefix', 'contentlinks_');
    variable_set('contentlinks_sr_property_mappings', json_encode(array()));
    variable_set('contentlinks_schema_org_mapped_entity_list', json_encode(array()));
    $content_fields = get_content_types_text_field_array();
    foreach ($content_fields as $type) {
        //get all the content types which are having file fields and  delte the variables
        $type_key = CONTENTLINKS_FIELD_PREFIX . $type['machine_name'];
        variable_set($type_key, 0);

        //get all the file fields
        foreach ($type['fields'] as $field) {
            $field_key = CONTENTLINKS_FIELD_PREFIX . $type['machine_name'] . "_" . $field['machine_name'];
            variable_set($field_key, 0);
        }
    }

    $t = get_t();

    drupal_set_message($t('ContentLinks settings created'));
}

/**
 * Implement hook_uninstall().
 */
function contentlinks_uninstall()
{
    //Initializing variables
    variable_del('contentlinks_*');
    $t = get_t();

    drupal_set_message($t('ContentLinks settings removed'));
}

/**
 * Implements hook_schema().
 *
 * @ingroup tablesort_example
 */
function contentlinks_schema()
{
    $schema['contentlinks_field_enrichment_mapping_table'] = array(
        'description' => 'Stores mapping record for each field to store enriched content.',
        'fields' => array(
            'id' => array(
                'description' => 'This column is the primary key id',
                'type' => 'serial',
                'length' => 10,
                'not null' => TRUE,
            ),
            'nid' => array(
                'description' => 'This column will have the corresponding node id',
                'type' => 'int',
                'length' => 10,
                'not null' => FALSE,
            ),
            'token' => array(
                'description' => 'This column will have the tem token for the record',
                'type' => 'varchar',
                'length' => 255,
                'not null' => FALSE,
            ),
            'type' => array(
                'description' => 'This column  will hold the content type',
                'type' => 'varchar',
                'length' => 32,
                'not null' => TRUE,
            ),
            'field_machine_name' => array(
                'description' => 'This column simply holds field machine name',
                'type' => 'varchar',
                'length' => 32,
                'not null' => TRUE,
            ),
            'field_index' => array(
                'description' => 'This column will have the corresponding field index',
                'type' => 'int',
                'length' => 10,
                'not null' => FALSE,
            ),
            'field_property' => array(
                'description' => 'This column simply holds the filed property',
                'type' => 'varchar',
                'length' => 32,
                'not null' => TRUE,
            ),
            'enriched_data_entities' => array(
                'description' => 'This column will hold the entities for the enhanced data',
                'type' => 'text',
                'size' => 'big',
                'not null' => FALSE,
            ),
            'enriched_microdata' => array(
                'description' => 'This column will hold all microdata from the enhanced response',
                'type' => 'text',
                'size' => 'big',
                'not null' => FALSE,
            )
        ),
        'primary key' => array('id'),
    );

    $schema['contentlinks_node_entities_table'] = array(
        'description' => 'Stores selected entities for every node.',
        'fields' => array(
            'id' => array(
                'description' => 'This column is the primary key id',
                'type' => 'serial',
                'length' => 10,
                'not null' => TRUE,
            ),
            'nid' => array(
                'description' => 'This column will have the corresponding node id',
                'type' => 'int',
                'length' => 10,
                'not null' => TRUE,
            ),
            'field_machine_name' => array(
                'description' => 'This column simply holds field machine name',
                'type' => 'varchar',
                'length' => 32,
                'not null' => TRUE,
            ),
            'field_index' => array(
                'description' => 'This column will have the corresponding field index',
                'type' => 'int',
                'length' => 10,
                'not null' => FALSE,
            ),
            'field_property' => array(
                'description' => 'This column simply holds the filed property',
                'type' => 'varchar',
                'length' => 32,
                'not null' => TRUE,
            ),
            'entity_uri' => array(
                'description' => 'This column will hold the entities URI',
                'type' => 'varchar',
                'length' => '255',
                'not null' => TRUE,
            )
        ),
        'primary key' => array('id'),
    );

    return $schema;
}
